/*
 * TSPGenome.cpp
 *
 *  Created on: 18.12.2014
 *      Author: che
 */

#include "TSPGenome.h"
#include "GA/Genome.h"
#include "GA/PermutationGenome.h"
namespace practical {
namespace ga {
TSPGenome::TSPGenome() :practical::ga::PermutationGenome(){

}
TSPGenome::TSPGenome(const std::vector<practical::ga::IntGene>& genes) :practical::ga::PermutationGenome(genes){

}

TSPGenome::TSPGenome(const Genome& other) :practical::ga::PermutationGenome(other){

}
double TSPGenome::fitness() const {
	//our return Value
	double sum = 0;
	/*
	 * We grab that instance from tsp::Ciries
	 * Remeber: it's a singelton.
	 */

	tsp::Cities a = tsp::Cities::instance();
	int start = 0;
	int end = genes[genes.size() - 1];
	sum += a.distance(start,end);

	for (int i=0; i < genes.size() -1; i++){
		start = genes[i];
		end = genes[i+1];
		sum += a.distance(start,end);
	}
	return (1/sum);
}


practical::ga::Genome* TSPGenome::shallowCopy() const {

	TSPGenome* a = new TSPGenome();
	return a;
}
} /* namespace ga */
} /* namespace practical */
