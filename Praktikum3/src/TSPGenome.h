/*
 * TSPGenome.h
 *
 *  Created on: 18.12.2014
 *      Author: che
 */

#ifndef SRC_TSPGENOME_H_
#define SRC_TSPGENOME_H_

#include "GA/PermutationGenome.h"
#include "GA/Genome.h"
#include "GA/PermutationGenome.h"
namespace practical {
namespace ga {

class TSPGenome: public PermutationGenome {
public:
	TSPGenome();
	TSPGenome(const std::vector<practical::ga::IntGene>& genes);
	TSPGenome(const Genome& other);
	double fitness() const;
	virtual ~TSPGenome();
	practical::ga::Genome* TSPGenome::shallowCopy() const;
};

} /* namespace ga */
} /* namespace practical */

#endif /* SRC_TSPGENOME_H_ */
