/*
 * TSPOrganism.cpp
 *
 *  Created on: 18.12.2014
 *      Author: che
 */

#include "GA/Organism.h"
#include "GA/Genome.h"
#include "GA/Gene.h"

namespace practical {
namespace ga {

TSPOrganism::TSPOrganism() :
		practical::ga::Organism() {
	setInitial_size();
}

TSPOrganism::TSPOrganism(unsigned int initial_size) :
		practical::ga::Organism(initial_size) {
	setInitial_size();
}

TSPOrganism::TSPOrganism(const Organism& other) :
		practical::ga::Organism(other) {
	setInitial_size();
}

void TSPOrganism::setInitial_size() {
	const tsp::Cities& cities = tsp::Cities::instance();

	this->initial_size = cities.getCities().size() * 10;
}

void TSPOrganism::initialize() {
	for (int i = 0; i < initial_size; i++) {
		createAGenome();
	}
}

void TSPOrganism::createAGenome() {
	const tsp::Cities& cities = tsp::Cities::instance();
	int AMOUNT_OF_CITIES = cities.getCities().size() * 10;
	int a[AMOUNT_OF_CITIES];
	for (int i = 0; i < AMOUNT_OF_CITIES; i++) {
		a[i] = i;
	}
	std::random_shuffle(&a[0], &a[AMOUNT_OF_CITIES - 1]);
	std::vector<practical::ga::IntGene> vIG;
	for (int i = 0; i < AMOUNT_OF_CITIES - 1; i++) {
		practical::ga::IntGene ig = a[i];
		vIG.push_back(ig);
	}
	Genome* g = Genome * (vIG);
	this->genomes.push_back(g);
}

TSPOrganism::~TSPOrganism() {
	// TODO Auto-generated destructor stub
}

practical::ga::Organism TSPOrganism::getOrganism() {
	practical::ga::Organism *ret = this;
	return ret;
}
} /* namespace ga */
} /* namespace practical */
