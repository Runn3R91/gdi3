/*
 * Organism.cpp
 *
 *  Created on: Nov 17, 2014
 *      Author: ahueck
 */

#include "Organism.h"
#include "Genome.h"
#include "Util.h"
#include <algorithm>
#include <numeric>
#include <cmath>
#include <iostream>

namespace practical {
namespace ga {

bool Organism::GenomeCmp::operator()(const Genome* lhs, const Genome* rhs){
	return lhs->fitness() < rhs->fitness();
}

Organism::Organism() :
		genomes(), mutateAfterCrossover(true), sorted(false), duelChildren(true) {
}

Organism::Organism(unsigned int initial_size) :
		genomes(), mutateAfterCrossover(true), sorted(false), duelChildren(true) {
}

Organism::Organism(const Organism& other) :
		genomes(), mutateAfterCrossover(other.mutateAfterCrossover), sorted(other.sorted), duelChildren(other.duelChildren) {
	copyGenomes(other);
}

const Organism& Organism::operator=(const Organism& rhs) {
	if (this != &rhs) {
		this->genomes = rhs.genomes;
		this->mutateAfterCrossover = rhs.mutateAfterCrossover;
		copyGenomes(rhs);
	}
	return *this;
}

void Organism::generate() {
	const unsigned int genome_count = this->genomes.size();
	std::deque<Genome*> new_genomes;

	while(genome_count > new_genomes.size()){
		std::pair<Genome*, Genome*> parents = this->selectParents();
		std::pair<Genome*, Genome*> children = parents.first->crossover(*parents.second);

		if (mutateAfterCrossover && util::randInt(1000) < 100) {
			children.first->mutate();
			children.second->mutate();
		}
		if(new_genomes.size() + 2 > genome_count) {
			if(children.second->duel(*children.first) == 1) {
				new_genomes.push_back(children.second);
				delete children.first;
			} else {
				new_genomes.push_back(children.first);
				delete children.second;			
			}
		} else {
			new_genomes.push_back(children.first);
			new_genomes.push_back(children.second);
		}
	}

	std::sort(new_genomes.begin(), new_genomes.end(), GenomeCmp());

	// best of last generation is kept.
	const Genome& new_generation_worst = *new_genomes.front();
	if(fittest()->duel(new_generation_worst) == 1) {
		delete new_genomes.front();
		new_genomes.pop_front();
		new_genomes.insert( 
            std::upper_bound(new_genomes.begin(), new_genomes.end(), fittest(), GenomeCmp())
			, fittest()->deepCopy()
		);
	}

	clearGenomes();
	genomes.swap(new_genomes);
	sorted = true;
}

double Organism::fitness() const {
	return fittest()->fitness();
}

const Genome* Organism::fittest() const {
	return genomes.back();
}

const Genome* Organism::worst() const {
	return genomes.front();std::pair <std::string,double> product;
}

std::pair<Genome*, Genome*> Organism::selectParents() {
	/*
	 * At first, we'll get the number of all Elements in the genomes,
	 * then we will choose up to 6 Elements to those.
	 * If there are less than 6 Elements in genomes, we will choose all elements.
	 * Then we will get the 2 best elements of this random generated list.
	 * And give them back as a pair.
	 * CWH
	 */
    int size = genomes.size();
    //Enter maximum selectable amount of elements here, at const MAX_SELECT.
    int const MAX_SELECT = 6;
    //int const AMOUNT_RET_VALUES = 2;
    int toSelect = MAX_SELECT > size? size : MAX_SELECT;

    //Initialize the random generator
    util::initRand();
    //Initialize the Genome deque to hold our selected Parents
    std::deque<Genome*> randomgenomes;

    /*
     * Create a vector, to hold our 6 random generated ints to get
     * the 6 (in case there are enough genomes) genomes we need.
     */

    //Initialize the vector we need
    std::vector<int> randomNumbers;
    //Reserve the memory we need for this.
    randomNumbers.reserve(toSelect);
    for (int i =0; i < toSelect; i++){
    	int randomInt = util::randInt(size,0);

    	//Let's check if this number is already in the array
    	for (int j =0; j < i; j++){
    		//if the number already got rolled, we will count i down for one.
    		if (randomNumbers[j] == randomInt){
    			i--;
    			//We don't need to check the rest of the numbers for our purpose.
    			break;
    		}
    	}
    }

    //Let's select some elements!
    for (int i = 0; i < randomNumbers.size(); i++){
    	Genome* g = genomes[randomNumbers[i]];
    	randomgenomes.push_back(g);
    }

    /*
     * Lets sort the deque of our selected genomes
     * Based on the sorting we will select the elements with the best fitness value
     * Idea came by looking at Organism::sort and Oragnism::fittest.
     */
    std::sort(randomgenomes.begin(), randomgenomes.end(), GenomeCmp());
    std::pair<Genome*, Genome*> retPair;
    //preparing the retPair.
    retPair.first = randomgenomes.back();
    retPair.second = randomgenomes[randomgenomes.size() - 2];

    return retPair;

}

void Organism::sort() {
	std::sort(genomes.begin(), genomes.end(), GenomeCmp());
	sorted = true;
}

void Organism::copyGenomes(const Organism& rhs) {
	std::deque<Genome*>::const_iterator iter = rhs.genomes.begin();
	for (; iter != rhs.genomes.end(); ++iter) {
		const Genome* original = *iter;
		this->genomes.push_back(original->deepCopy());
	}
}

const std::deque<Genome*>& Organism::getGenomes() const {
	return genomes;
}

void Organism::clearGenomes() {
	std::deque<Genome*>::iterator iter = genomes.begin();
	for (; iter != genomes.end(); ++iter) {
		delete *iter;
	}
	genomes.clear();
}

Organism::~Organism() {
	clearGenomes();
}

std::ostream& operator<<(std::ostream& os, const Organism& organism) {
    os << "<[";
    std::deque<practical::ga::Genome*>::const_iterator iter = organism.getGenomes().begin();
	for (; iter < organism.getGenomes().end() - 1; ++iter) {
		os << *(*iter) << " | ";
	}
	return os << *(*iter) << "]>";
}

} /* namespace ga */
} /* namespace practical */
