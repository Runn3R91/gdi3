/*
 * PermutationGenome.h
 *
 *  Created on: 08.12.2014
 *      Author: che
 */

#ifndef SRC_GA_PERMUTATIONGENOME_H_
#define SRC_GA_PERMUTATIONGENOME_H_

#include "Genome.h"
#include "Util.h"
#include "Gene.h"


namespace practical {
	namespace ga {

		class PermutationGenome: public practical::ga::Genome {
		protected:

		public:
			PermutationGenome(const std::vector<practical::ga::IntGene>& genes);
			PermutationGenome();
			PermutationGenome(const Genome& other);
			virtual ~PermutationGenome();
			void mutate();
			std::pair<Genome*, Genome*> crossover(const Genome& other) const;
		};
	}
}
#endif /* SRC_GA_PERMUTATIONGENOME_H_ */
