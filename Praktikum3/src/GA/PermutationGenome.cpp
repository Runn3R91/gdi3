/*
 * PermutationGenome.cpp
 *
 *  Created on: 08.12.2014
 *      Author: che
 */

#include "PermutationGenome.h"
#include "Util.h"
#include "Genome.h"
#include "Gene.h"
namespace practical {
	namespace ga {
		PermutationGenome::PermutationGenome(const std::vector<practical::ga::IntGene>& genes) :practical::ga::Genome(genes) {

		}

		PermutationGenome::~PermutationGenome() {
			// TODO Auto-generated destructor stub
		}

		PermutationGenome::PermutationGenome() :practical::ga::Genome(){

		}
		PermutationGenome::PermutationGenome(const Genome& other) :practical::ga::Genome(other) {

		}
		/*
		 * @TLDR: Change 2 Elements
		 *
		 * @Return: Void
		 * Changes 2 random Elements in this genome.
		 * Will modify the genome itself.
		 * CWH
		 */
		void PermutationGenome::mutate(){
			//Inizialize the Random generator:
			practical::ga::util::initRand();
			//Get the size of our gene array.
			int size = genes.size();
			//Let's get the 2 Random values that will change their place
			int RandomInt = practical::ga::util::randInt(size, 0);
			int RandomInt2 = practical::ga::util::randInt(size, 0);
			//Check if both are the same....
			while (RandomInt == RandomInt2){
				RandomInt2 = practical::ga::util::randInt(size, 0);
			}

			//We got both indexes. Now we will exchange the elements.
			std::iter_swap(genes.begin() + RandomInt, genes.begin() + RandomInt2);


			//Finished. In under 2 hours of work! Hooray!
		}
		/**
		 * @TLDR: Genomes have Sex.
		 *
		 * @Param genome: The gene we want to cross with.
		 *
		 * @return deque of 2 genomes with the new Children in no particluar Order.
		 * CWH
		 */
		std::pair<Genome*, Genome*> PermutationGenome::crossover(const Genome& other) const {
			//Intialize the Random generator:
			practical::ga::util::initRand();
			int size = this->genes.size();

			//This will get us the index where we will do the crossover.
			int crossOverPoint = practical::ga::util::randInt(size, 0);
			practical::ga::Genome* retGenome1 = other.shallowCopy();
			practical::ga::Genome* retGenome2 = other.shallowCopy();

			/*
			 * This will get the first part of our new genomes dones.#
			 * It will take the beginning of each genome and save it to our ret genomes.
			 */
			for (int i = 0; i < crossOverPoint; i++){
				retGenome1->genes.push_back(this->genes[i]);
				retGenome2->genes[i] = other.getGenes()[i];
			}

			/*
			 * Now comes the complex part:
			 * We will have to copy the other genome that isn't the first partent,
			 * and copy it from the beginning to end, to the new retGenome.
			 * Problem: Each int is only allowed to appear once.
			 *
			 */

			//j will point to the current Element we want to check for a copy in the retGenome.
			int j = 0;
			//i will always point to the current Element we want to set.
			for (int i = crossOverPoint; i < genes.size(); i++){
				//We assume that this Element is the right one.
				retGenome1->genes[i] = other.genes[j];
				//Check if we have a double Element here.
				for (int e = 0; e < i; e++){
					//If we have a Element twice in our genomes.....
					if (retGenome1->genes[e] == other.genes[j]){
						//We will try to write the genes[i] with an other
						//Value of genome2.genes[j]
						i--;
						break;
					}
				}
				//We raise j for one.
				j++;
			}
			j = 0;
			for (int i = crossOverPoint; i < this->genes.size(); i++){
				retGenome2->genes[i] = this->genes[j];

				for (int e = 0; e < i; e++){
					if (retGenome2->genes[e] == this->genes[j]){
						i--;
						break;
					}
				}
				j++;
			}
			/*
			 * Here we create our return deque.
			 */
			std::pair<Genome*, Genome*> retPair;
			retPair.first = retGenome1;
			retPair.second = (retGenome2);

			return retPair;
		}
	}
}


