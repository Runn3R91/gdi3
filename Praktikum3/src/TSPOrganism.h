/*
 * TSPOrganism.h
 *
 *  Created on: 18.12.2014
 *      Author: che
 */

#ifndef SRC_TSPORGANISM_H_
#define SRC_TSPORGANISM_H_

#include "GA/Organism.h"
#include "GA/Genome.h"
#include "GA/Gene.h"

namespace practical {
namespace ga {

class TSPOrganism: public Organism {
protected:
	unsigned int initial_size;
public:
	TSPOrganism();

	TSPOrganism(unsigned int initial_size);
	TSPOrganism(const Organism& other);
	TSPOrganism();

	void setInitial_size();
	void initialize();
	void createAGenome();

	Organism getOrganism();
	virtual ~TSPOrganism();
};

} /* namespace ga */
} /* namespace practical */

#endif /* SRC_TSPORGANISM_H_ */
